var sandwich = document.getElementById("sandwich"),
      aside = document.getElementById("aside"),
      asideToggle = document.getElementById("aside-toggle"),
      submenu = document.querySelectorAll(".nav .is--submenu"),
      profile = document.getElementById("profile"),
      btnSearch = document.getElementById("search-btn"),
      headerRow = document.getElementById("header-row");

function sandwichHandler() {
    var root = document.getElementById("root");
    if ( root.classList.contains("header--active") ) {
        root.classList.remove("header--active");
    } else {
        root.classList.add("header--active");
    }
}

function toggleClass(el) {
    if ( el.parentElement.classList.contains("is--active") == false ) {
        el.parentElement.classList.add("is--active");
    } else {
        el.parentElement.classList.remove("is--active");
    }
}

function toggleMenu(element) {
    if ( element.classList.contains("is--open") == false ) {
        element.classList.add("is--open");
    } else {
        element.classList.remove("is--open");
    }
}

document.addEventListener("DOMContentLoaded", function(){

    sandwich.addEventListener("click", function(){
        sandwichHandler();
        if ( profile.parentElement.classList.contains("is--active") == true ) {
            toggleClass(profile);
        }
    });

    if ( aside != null ) {
        aside.addEventListener("click", function(){
            if ( asideToggle.classList.contains("aside--open") == true ) {
                asideToggle.classList.remove("aside--open");
                asideToggle.classList.add("aside--close");
            } else {
                asideToggle.classList.remove("aside--close");
                asideToggle.classList.add("aside--open");
            }
        });
    }

    profile.addEventListener("click", function(e){
        e.preventDefault();
        toggleClass(profile);
    });

    btnSearch.addEventListener("click", function(e){
        e.preventDefault();
        if ( window.innerWidth >414 ) {
            sandwich.click();
        } else {
            toggleClass(headerRow);

            if ( profile.parentElement.classList.contains("is--active") == true ) {
                toggleClass(profile);
            }
        }
    });

    
    Array.prototype.slice.call(submenu).forEach(function(currentValue) {
        currentValue.addEventListener( 'click', function(e){e.preventDefault(); toggleMenu(this);} );
    });

});

